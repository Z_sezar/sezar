🛑آموزش ساخت تبلیغاتچی🛑

⭕️ابتدا 5 نکته را باید رعایت کنید:👇

1⃣ اکانت ربات ها حتما اوکراین و یا عربستان سعودی باشد .
2⃣ دسترسی اینلاین برای بات هلپر ( Api ) حتما فعال کنید .
3⃣ ربات هلپر (Api) را با اکانت تبچی (Cli) استارت کنید .
4⃣ در هر روز بیشتر از یک پست با ربات ها ارسال ننمایید , از ارسال هر پست تا پست جدید 25 ساعت وقت بگذارید .
5⃣ ایپی سرور شما حتما دست اول باشد و استفاده نشده باشد , ترجیحا سرور امریکا باشد.
➖➖➖➖➖➖➖➖
1⃣ ابتدا فایلی را که در بالا دادیم را اپلود میکنید داخل یوزر مورد نظر خود.
✅ سپس فایل Helper.lua داخل پوشه All را باز میکنید.
➖➖➖➖➖➖➖➖
1⃣ در لاین های مشخص شده ، مشخصات خواسته شده را ست کنید.
2⃣ لاین 10 توکن ، لاین 15 آیدی عددی تبچی شماره 1 ، لاین 16 آیدی عددی خودتون.
3⃣ سپس فایل را سیو کنید.
➖➖➖➖➖➖➖➖
✅حالا فایل Zcli.lua را باز کنید.
1⃣ در لاین های مشخص شده ، مشخصات خواسته شده را ست کنید.
2⃣ لاین 3 آیدی عددی بات هلپر (Api) ، لاین 4 آیدی عددی کانال خود ، لاین 5 آیدی (یوزرنیم) کانال خود.
3⃣ سپس فایل را سیو کنید.
#_توی_فایلهای
(Zcli2.lua و Zcli3.lua)
فقد ایدی کانالتونو ست کنید انجام بدید.
➖➖➖➖➖➖➖➖
1⃣حال ترمینال باز کنید و دستورات زیر را به ترتیب وارد کنید( #شماره1):👇
cd Z-sezar
chmod +x launch
./launch install
2⃣منتظر بمانید تا نصب تکمیل شود سپس با دستور زیر ربات تبچی شماره 1 را نصب کنید :👇
./launch zcli
3⃣پس از زدن این دستور ابتدا آیدی عددی خود را وارد نمونه  و سپس شماره اکانت ربات و کد .
4⃣حال ترمینال جدید باز کنید ( ترمینال قبلی را نبندید ! ) و دستورات زیر را به ترتیب وارد کنید : 👇
cd Z-sezar
./launch menu
5⃣اگر بدون هیچ مشکل اجرا شد برای ربات دستور Menu را ارسال نمایید و اگر مشکلی نداشت ترمینال هارو ببندید و ترمینال جدید باز کنید : 👇
./launch online
✅و منتظر بمانید تا لانچ تمام شود و سپس ترمینال را ببندید .
➖➖➖➖➖➖➖➖
1⃣برای ساخت ربات ( #شماره2) ترمینال باز کنید و دستورات زیر را وارد نمایید :👇
cd Z-sezar
killall tmux
./launch zcli2
2⃣آیدی عددی ادمین و شماره ربات و کد را وارد کنید و تمام .
3⃣حال ترمینال را ببندید و ترمینال جدید باز کنید , دستورات زیر را وارد نمایید :👇
cd Z-sezar
./launch online
➖➖➖➖➖➖➖➖
1⃣برای ساخت ربات ( #شماره3) ترمینال باز کنید و دستورات زیر را وارد نمایید :👇
cd Z-sezar
killall tmux
./launch zcli3
2⃣آیدی عددی ادمین و شماره ربات و کد را وارد کنید و تمام .
3⃣حال ترمینال را ببندید و ترمینال جدید باز کنید , دستورات زیر را وارد نمایید :👇
cd Z-sezar
./launch online
⭕️⭕️⭕️⭕️⭕️⭕️⭕️
#لانچ_همگانی
اول این کد رو توی ترمینال بزنید👇
killall tmux
cd Z-sezar
./launch menu
و توی ترمینال جدید این کد رو بزنید👇
killall tmux
cd Z-sezar
./launch online